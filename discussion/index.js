// Create simpl server

const express = require('express');

const app = express();

const port = 4000;

// Middlewares

app.use(express.json())
app.use(express.urlencoded({
	extended: true}));

// mock database
let users = [
	{
		email: "sasageyyo@mail.com",
		username: "erwinNoArm",
		password: "deads",
		isAdmin: false
	},
	{
		email: "mrclean@mail.com",
		username: "LeviAckerman",
		password: "alive",
		isAdmin: true
	},
	{
		email: "redScarf@mail.com",
		username: "Mikasaaaaaa",
		password: "wheresEreh",
		isAdmin: false
	},
];

let loggedUser;

app.get('/', (req, res) => {
	res.send('Hello World')

});


// MINI ACTIVITY
// 1. Make a route /hello as an endpoint
// 2. Send a message saying hello from btch 145

app.get('/hello', (req, res) => {
	res.send('hello from batch 145')

});

// POST METHOD

// Mini-Activity
// 1. Change message from POST METHOD
// 2. res.send = Hello, I am <nameFromRequst>, my age is <ageFromRequest>.  I could be described as <descriptionFromREquest>

app.post('/', (req, res) => {

	console.log(req.body)
	res.send(`Hello, I am ${req.body.name}, my age is ${req.body.age}.  I could be described as ${req.body.description}`)

});


// Register route
app.post('/users', (req, res) => {

	console.log(req.body);

	// will handle new user
	let newUser  = {
		// to access body
		email: req.body.email,
		username: req.body.username,
		password: req.body.password,
		isAdmin: req.body.isAdmin
	};

	// to push
	users.push(newUser);
	console.log(users);

	res.send(`User ${req.body.username} has successfully registered`)
});



// login route

app.post('/users/login', (req, res) => {

	console.log(req.body);

	// hahanapin ang kaparehas na username at pssword
	let foundUser = users.find((user) => {

		return user.username === req.body.username && user.password === req.body.password
	})


	if(foundUser !== undefined){
		// kapag nahanap....
		let foundUserIndex = users.findIndex((user) => {
			return user.username === foundUser.username
		});
		
		foundUser.index = foundUserIndex

		loggedUser = foundUser

		res.send(`Thank you for logging in.`)

	} else {

		loggedUser = foundUser
		res.send('Sorry, wrong credentials')
	}
});



// Change Password
app.put('/users/change-password', (req, res) => {

	let message;

	for(let i = 0; i < users.length; i++){

		if(req.body.username == users[i].username){

			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been changed`

			break;

		} else {

			message = `User does not exist`
		}
	}

	res.send(message)
})


// THIS IS THE ACTIVITY PART

// 1.0 and 2.0
app.get('/home', (req, res) => {
	res.send('Welcome to the home page')

});


// 3.0 and 4.0
app.get('/users', (req, res) => {
	res.send(users)

});

// 5.0 and 6.0
app.delete('/delete-user',(req, res) => {
	res.send(`Username ${req.body.username} has been deleted`)
});



app.listen(port, () => console.log(`Server running at port ${port}`));